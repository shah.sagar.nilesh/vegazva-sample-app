#### Sample app to show how to decouple and efficiently store an object.

#### Each class has comments explaining the purpose of it and any of its members.

#### The main concept is to use interface and abstract class to decouple and make the app extensible.

[/app/src/main/java/com/sagarnileshshah/sampleapp/data/repository](https://gitlab.com/shah.sagar.nilesh/vegazva-sample-app/tree/master/app/src/main/java/com/sagarnileshshah/sampleapp/data/repository) - contains the interfaces and the classes to interact with the data stores

[/app/src/main/java/com/sagarnileshshah/sampleapp/data/models](https://gitlab.com/shah.sagar.nilesh/vegazva-sample-app/tree/master/app/src/main/java/com/sagarnileshshah/sampleapp/data/models) - contains the base model and implementations of the base model such as `Comment`
