package com.sagarnileshshah.sampleapp.data.repository;

import com.sagarnileshshah.sampleapp.data.models.BaseModel;

/**
 * Class of the main data store service that implements the {@link DataRepository}. This class
 * is the primary class to interact with for getting and storing data.
 * This class accordingly delegates calls to other types of data store services such as
 * Remote data store, Local data store, etc.
 */
public class DataRepositoryImpl implements DataRepository {

    private static DataRepositoryImpl dataRepositoryImpl;
    private DataRepository remoteDataRepository;

    private DataRepositoryImpl(DataRepository remoteDataRepository) {
        this.remoteDataRepository = remoteDataRepository;
    }

    public synchronized static DataRepository getInstance(
            DataRepository remoteDataRepository) {
        if (dataRepositoryImpl == null) {
            dataRepositoryImpl = new DataRepositoryImpl(remoteDataRepository);
        }
        return dataRepositoryImpl;
    }

    @Override
    public <T extends BaseModel> void store(T object, Callback<String> onSuccess,
            Callback<String> onError) {
        remoteDataRepository.store(object, onSuccess, onError);
    }
}
