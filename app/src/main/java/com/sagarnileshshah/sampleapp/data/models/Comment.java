package com.sagarnileshshah.sampleapp.data.models;


/**
 * Sample implementation of Comment model
 */
public class Comment extends BaseModel {

    private String content;
    private String username;

    public Comment() {
    }

    public Comment(String content, String username) {
        this.content = content;
        this.username = username;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return String.format("content: %s, username: %s", content, username);
    }

    @Override
    public String getEndpoint() {
        return "comment";
    }

}
