package com.sagarnileshshah.sampleapp.data.repository;


/**
 * Generic class to send specified type of data via callbacks.
 */
public interface Callback<T> {

    void call(T object);
}
