package com.sagarnileshshah.sampleapp.data.models;


import com.sagarnileshshah.sampleapp.data.repository.DataRepository;

/**
 * Abstract base model to be extended by all models. It contains common methods that will be called
 * by other classes such as {@link DataRepository} to do operations on any model of type
 * BaseModel.
 */
public abstract class BaseModel {

    protected int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    // Method to get endpoint to be used by data store service to store model to a relevant location
    public abstract String getEndpoint();

}

