package com.sagarnileshshah.sampleapp.data.repository;


import com.sagarnileshshah.sampleapp.data.models.BaseModel;

/**
 * Sample implementation of a remote data stores service. This can be a REST or a Parse or any other
 * kind of remote service.
 */
public class RemoteDataRepositoryImpl implements DataRepository {

    private static RemoteDataRepositoryImpl remoteDataRepository;

    private RemoteDataRepositoryImpl() {
    }

    public synchronized static RemoteDataRepositoryImpl getInstance() {
        if (remoteDataRepository == null) {
            remoteDataRepository = new RemoteDataRepositoryImpl();
        }
        return remoteDataRepository;
    }

    /**
     * Object can be serialized using Gson (https://github.com/google/gson)
     * Objects can be serialized based on the type of payload the service accepts.
     *
     * If this is a REST service, then this method can serialize the object into a JSON payload
     * and POST the payload to the end location defined by the
     * {@link BaseModel#getEndpoint())
     *
     * If this is a Parse service, then this method can serialize the object and use the
     * Parse SDK to store
     * the object at the location defined by {@link BaseModel#getEndpoint()).
     *
     * Response can be returned via {@link Callback} to the calling class
     * such as Activity/Fragment
     */
    @Override
    public <T extends BaseModel> void store(T object, Callback<String> onSuccess,
            Callback<String> onError) {

    }
}
