package com.sagarnileshshah.sampleapp.data.repository;


import com.sagarnileshshah.sampleapp.data.models.BaseModel;

/**
 * Interface that should be implemented by any data store service such as a REST or Parse service.
 * Methods should accept a generic type that extends {@link BaseModel} to allow operations on all
 * model types of BaseModel.
 */
public interface DataRepository {

    /**
     * Method to store the model in a data store such as REST/Parse/Local data store.
     * Callbacks used to send response back to the calling class such as Activity/Fragment
     */
    <T extends BaseModel> void store(T object, Callback<String> onSuccess,
            Callback<String> onError);

}