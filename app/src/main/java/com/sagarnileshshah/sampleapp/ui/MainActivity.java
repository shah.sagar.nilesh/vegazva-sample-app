package com.sagarnileshshah.sampleapp.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.sagarnileshshah.sampleapp.R;
import com.sagarnileshshah.sampleapp.data.models.Comment;
import com.sagarnileshshah.sampleapp.data.repository.Callback;
import com.sagarnileshshah.sampleapp.data.repository.DataRepository;
import com.sagarnileshshah.sampleapp.data.repository.DataRepositoryImpl;
import com.sagarnileshshah.sampleapp.data.repository.RemoteDataRepositoryImpl;

public class MainActivity extends AppCompatActivity {

    DataRepository dataRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Sample Implementation
        initMemberVariables();
        initData();
    }

    private void initMemberVariables() {
        // Instantiate a type of DataRepository
        dataRepository = DataRepositoryImpl.getInstance(
                RemoteDataRepositoryImpl.getInstance());

    }

    public void initData() {

        // Instantiate a Comment model of BaseModel type
        Comment comment = new Comment("Hello World", "Sagar");

        Callback<String> onSuccess = new Callback<String>() {

            @Override
            public void call(String object) {
                // Do something on successful operation such as update View, show Toast, etc.
            }
        };

        Callback<String> onError = new Callback<String>() {
            @Override
            public void call(String object) {
                // Do something on error such as update View, show Toast, etc.
            }
        };

        // Store the comment asynchronously and use callbacks to get response
        dataRepository.store(comment, onSuccess, onError);
    }
}
